#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "../src/parse.h"

char** read_buffer;
int read_pos = 0;
ssize_t read(int fd, void *buf, size_t count) {
	if (read_buffer[read_pos] != NULL) {
		strncpy(buf, read_buffer[read_pos], count);
		read_pos++;
		return strlen(buf);
	}
	else {
		return 0;
	}
}

//just check no crash
void test_read_simple() {
	char* buffer[] = {
		"one"
		,NULL
	};
	read_buffer = buffer;
	read_pos = 0;

	char* result = read_stdin();
	assert(0 == strcmp(result, "one"));
	free(result);
}

//check simple query with no parameters returned
void test_parse_noargs() {
	char* query = "select * from foo";
	char** result = rg(query);
	assert(0 == strcmp(result[0], query));
	assert(!result[1]);
	rg_free(result);
}

//test simple query with single param returned correctly
void test_parse_single() {
	char* query = "select * from foo where id=?id";
	char* expected = "select * from foo where id=?";
	char** result = rg(query);
	assert(0 == strcmp(result[0], expected));
	assert(0 == strcmp(result[1], "id"));
	assert(!result[2]);
	rg_free(result);
}

//test simple query with multiple params returned correctly
void test_parse_multiple() {
	char* query = "select * from foo where id=?id and name=?name";
	char* expected = "select * from foo where id=? and name=?";
	char** result = rg(query);
	assert(0 == strcmp(result[0], expected));
	assert(0 == strcmp(result[1], "id"));
	assert(0 == strcmp(result[2], "name"));
	assert(!result[3]);
	rg_free(result);
}

//test trailing test after last param included
void test_parse_trailing() {
	char* query = "select * from foo where id=?id and name='foo'";
	char* expected = "select * from foo where id=? and name='foo'";
	char** result = rg(query);
	assert(0 == strcmp(result[0], expected));
	assert(0 == strcmp(result[1], "id"));
	assert(!result[2]);
	rg_free(result);
}

int main(int argc, char** argv) {
	test_read_simple();
	test_parse_noargs();
	test_parse_single();
	test_parse_multiple();
	test_parse_trailing();
}
