#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "../src/pbuffer.h"

void simple_test() {
	char* pbuffer = NULL;
	size_t pbuffer_len = 0;
	size_t pbuffer_offset = 0;
	const int i = 23;
	const long j = 1000000;
	const char* foo = "123456789123456789123456789123456789";


	pb_add(&pbuffer, &pbuffer_len, &pbuffer_offset, &i, sizeof(int));

	assert(pbuffer_len >= sizeof(int));
	assert(pbuffer_offset == sizeof(int));
	assert(23 == (int)pbuffer[0]);

	pb_add(&pbuffer, &pbuffer_len, &pbuffer_offset, &j, sizeof(long));

	assert(pbuffer_len >= sizeof(int) + sizeof(long));
	assert(pbuffer_offset == sizeof(int) + sizeof(long));
	assert(1000000 == *(long*)(pbuffer + 4));

	pb_add(&pbuffer, &pbuffer_len, &pbuffer_offset, foo, strlen(foo));

	char foo_out[64];
	memcpy(foo_out, pbuffer + sizeof(int) + sizeof(long), strlen(foo) + 1);
	assert(0 == strcmp(foo, foo_out));

	free(pbuffer);
}

void return_value_test() {
	char* pbuffer = NULL;
	size_t pbuffer_len = 0;
	size_t pbuffer_offset = 0;
	const int i = 23;
	const long j = 1000000;
	const char* foo = "123456789123456789123456789123456789";

	int* r1 = pb_add(&pbuffer, &pbuffer_len, &pbuffer_offset, &i, sizeof(int));
	assert(23 == *r1);

	long* r2 = pb_add(&pbuffer, &pbuffer_len, &pbuffer_offset, &j, sizeof(long));
	assert(1000000 == *r2);

	char* r3 = pb_add(&pbuffer, &pbuffer_len, &pbuffer_offset, foo, strlen(foo));
	assert(0 == strcmp(foo, r3));

	free(pbuffer);
}

int main(int argc, char** argv) {
	simple_test();
	return_value_test();
}
