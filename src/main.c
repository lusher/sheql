#include <stdlib.h>
#include "db.h"
#include "parse.h"

int main(int argc, char** argv) {
	char* query = read_stdin();
	char** pquery = rg(query);
	db_exec(pquery);

	free(query);
	rg_free(pquery);
	return 0;
}
