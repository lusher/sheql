#pragma once
#include <stdlib.h>
#include <stdint.h>

void* pb_add(char** pbuffer, size_t* pbuffer_len, size_t* offset, const void* value, const size_t size);
