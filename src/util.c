#include "sql.h"

long hextolong(unsigned char* val)
{
	long result=0;
	int last=1;
	int current = 0;
	int n=0;
	int d=0;

	for(int i=0; i <= SQL_MAX_NUMERIC_LEN; i++)
	{
		current = (int)val[i];
		n= current % 16;
		d= current / 16;

		result += last* n;   
		last = last * 16;   
		result += last* d;
		last = last * 16;   
	}
	return result;
}

