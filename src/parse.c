#define _GNU_SOURCE 

#include <stdlib.h>
#include <string.h>
#include <regex.h>
#include <unistd.h>

char* read_stdin() {
	const size_t alloc_increment = 512;
	const size_t read_size = 255;
	char* buffer = malloc(sizeof(char) * alloc_increment);
	size_t buffer_length = alloc_increment;
	size_t total_read = 0;

	size_t bytes_read = 0;
	while ((bytes_read = read(STDIN_FILENO, buffer + total_read, read_size)) > 0) {
		total_read += bytes_read;
		if (total_read + read_size >= buffer_length) {
			buffer_length += alloc_increment;
			buffer = realloc(buffer, sizeof(char) * buffer_length);
		}
	}
	buffer[total_read] = '\0';

	return buffer;
}

char** rg(const char* buffer) {
	size_t matches_length = 8;
	char** matches = calloc(sizeof(char*),  matches_length);
	char* new_query = strdup(buffer);
	matches[0] = new_query;
	int result = 0;
	regex_t regex;
	regmatch_t match;
	result = regcomp(&regex, "\\?[[:alnum:]]+",  REG_EXTENDED);
	int write_offset = 0;
	size_t i = 1;
	while ((result = regexec(&regex, buffer, 1, &match, 0)) == 0) {

		//reallocate if the matches buffer isn't big enough
		if (i >= matches_length) {
			matches_length += 8;
			matches = reallocarray(matches, sizeof(char*), matches_length);
		}

		int match_length = match.rm_eo - match.rm_so - 1;
		write_offset += match.rm_so + 1;
		//copy the remainder of the original query to the new, minus the paramter name
		strcpy(new_query + write_offset, buffer + match.rm_eo);
		matches[i] = strndup(buffer + match.rm_so + 1, match_length);
		buffer += match.rm_eo;
		i++;
	}
	
	regfree(&regex);
	return matches;
}

void rg_free(char** result) {
	for (int i = 0; result[i] != NULL; i++)
		free(result[i]);
	free(result);
}
