#define _GNU_SOURCE

#include <stdio.h>
#include <sql.h>
#include <sqlext.h>
#include <sqltypes.h>
#include <string.h>
#include <math.h>
#include <stdbool.h>

#include "pbuffer.h"
#include "util.h"

void db_list_drivers();
void db_list_data_sources();
void db_print(SQLHSTMT stmt);
void db_print_error(SQLHANDLE handle, SQLSMALLINT handle_type);
bool db_exec_stmt(SQLHSTMT stmt, char** query);

#define check(function, handle, handle_type) { \
	ret = function; \
	if (!SQL_SUCCEEDED(ret)) { db_print_error(handle, handle_type); goto cleanup; } \
	/*extract_error("SQLDriverConnect", dbc, SQL_HANDLE_DBC); */ \
}

int db_exec(char** query) {
	SQLRETURN ret = 0;
	SQLHENV env = NULL;
	SQLHDBC dbc = NULL;
	SQLHSTMT stmt = NULL;
	char* connStr = NULL;
	int connStrLen = 0;

	check(SQLAllocHandle(SQL_HANDLE_ENV, SQL_NULL_HANDLE, &env), env, SQL_HANDLE_ENV);
  check(SQLSetEnvAttr(env, SQL_ATTR_ODBC_VERSION, (void *) SQL_OV_ODBC3, 0), env, SQL_HANDLE_ENV);

	char* dsn = getenv("SHEQL_DSN");
	char* conn = getenv("SHEQL_CONN");
	if (dsn != NULL) {
		connStrLen = asprintf(&connStr, "DSN=%s", dsn);
	}
	else if (conn != NULL)
		connStrLen = asprintf(&connStr, "%s", conn);
	else {
		fprintf(stderr, "Error: SHEQL_DSN and SHEQL_CONN undefined, connection information must be provided\n");
		goto cleanup;
	}
	if (connStrLen <= 0) {
		fprintf(stderr, "Error: error allocation connection string\n");
		goto cleanup;
	}

	check(SQLAllocHandle(SQL_HANDLE_DBC, env, &dbc), env, SQL_HANDLE_ENV);
	check(SQLDriverConnect(dbc, NULL, (SQLCHAR*)connStr, connStrLen, NULL, 0, NULL, SQL_DRIVER_COMPLETE), dbc, SQL_HANDLE_DBC);

	check(SQLAllocHandle(SQL_HANDLE_STMT, dbc, &stmt), dbc, SQL_HANDLE_DBC);
	//check(SQLPrepare(stmt, (SQLCHAR*)query[0], SQL_NTS), stmt, SQL_HANDLE_STMT);
	ret = SQLPrepare(stmt, (SQLCHAR*)query[0], SQL_NTS);
	if (ret != SQL_SUCCESS) {
		db_print_error(stmt, SQL_HANDLE_STMT);
		goto cleanup;
	}
	if (db_exec_stmt(stmt, query))
		db_print(stmt);

	cleanup:
		SQLFreeHandle(SQL_HANDLE_STMT, stmt);
		SQLDisconnect(dbc);
		free(connStr);
		SQLFreeHandle(SQL_HANDLE_DBC, dbc);
		SQLFreeHandle(SQL_HANDLE_ENV, env);
	
	return ret;
}



bool db_exec_stmt(SQLHSTMT stmt, char** query) {
	SQLRETURN ret = 0;
	SQLSMALLINT paramsCount = 0;
	SQLNumParams(stmt, &paramsCount);

	char* pb = NULL;
	size_t pb_len = 0;
	size_t pb_offset= 0;

	for (SQLSMALLINT i = 1; i < paramsCount +1; i++) {
		SQLSMALLINT paramType = 0;
		SQLULEN paramLength = 0;
		SQLSMALLINT paramDigits = 0;
		SQLSMALLINT paramNullable = 0;
		check(SQLDescribeParam(stmt, i, &paramType, &paramLength, &paramDigits, &paramNullable), stmt, SQL_HANDLE_STMT);
		if (query[i-1] == NULL) {
			fprintf(stderr, "Statement has more parameters than provided\n");
			goto cleanup;
		}

		char* value_env = getenv(query[i]);
		if (value_env == NULL) {
			fprintf(stderr, "Variable '%s' not defined\n", query[i]);
			goto cleanup;
		}

		switch (paramType) {
			case SQL_INTEGER: {
				long value = atol(value_env);
				void* ref = pb_add(&pb, &pb_len, &pb_offset, &value, sizeof(long));
				check(SQLBindParameter(stmt, i, SQL_PARAM_INPUT, SQL_C_LONG, SQL_INTEGER, (SQLULEN)NULL, -1, ref, (SQLULEN)NULL, NULL), NULL, SQL_HANDLE_STMT);
				break;
			}
			case SQL_LONGVARCHAR: {
				void* ref = pb_add(&pb, &pb_len, &pb_offset, value_env, sizeof(long));
				check(SQLBindParameter(stmt, i, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_LONGVARCHAR, (SQLULEN)NULL, -1, ref, (SQLULEN)NULL, NULL), stmt, SQL_HANDLE_STMT);
				break;
			}
		}
	}
	ret = SQLExecute(stmt);
	if (ret != SQL_SUCCESS) {
		db_print_error(stmt, SQL_HANDLE_STMT);
		goto cleanup;
	}
	cleanup:
	return true;
}

void db_print(SQLHSTMT stmt) {
	SQLSMALLINT cols = 0;
	SQLNumResultCols(stmt, &cols);
	SQLSMALLINT fetch = SQLFetch(stmt);
	while (SQL_SUCCEEDED(fetch)) {
		for (SQLUSMALLINT i = 1; i <= cols; i++) {
			SQLCHAR colName[256];
			SQLSMALLINT colNameL = 0;
			SQLSMALLINT colType = 0;
			SQLULEN colSize = 0;
			SQLSMALLINT colDigits = 0;
			SQLSMALLINT colNullable = 0;

			SQLDescribeCol(stmt, i, (SQLCHAR*)&colName, sizeof(colName), &colNameL, &colType, &colSize, &colDigits, &colNullable);
			if (i > 1)
				printf("\t");
			SQLLEN indicator;
			if (colType == SQL_INTEGER) {
				SQLINTEGER val = 0;
 				SQLGetData(stmt, i, SQL_INTEGER, &val, sizeof(val), &indicator);
				printf("%i", val);
			}
			else if (colType == SQL_VARCHAR) {
				char val[256];
 				SQLGetData(stmt, i, SQL_C_CHAR, &val, sizeof(val), &indicator);
				fprintf(stdout, "%s", (char*)&val);
			}
			else if (colType == SQL_NUMERIC) {
				SQL_NUMERIC_STRUCT val;
 				SQLGetData(stmt, i, SQL_NUMERIC, &val, sizeof(val), &indicator);
				long intval = hextolong(val.val);
				//double power = pow10((double)val.precision);
				long power = powl(10, val.scale);
				printf("%lu.%lu", intval / power, intval % power);
			}

		}
		printf("\n");
		fetch = SQLFetch(stmt);
	}


}

void db_list_drivers() {
	SQLHENV env;
	SQLAllocHandle(SQL_HANDLE_ENV, SQL_NULL_HANDLE, &env);
  SQLSetEnvAttr(env, SQL_ATTR_ODBC_VERSION, (void *) SQL_OV_ODBC3, 0);

	SQLSMALLINT direction = SQL_FETCH_FIRST;
	SQLCHAR driver[256];
	SQLCHAR attr[256];
	SQLRETURN ret;
	SQLSMALLINT driver_ret;
	SQLSMALLINT attr_ret;
  while(SQL_SUCCEEDED(ret = SQLDrivers(env, direction,
                                       driver, sizeof(driver), &driver_ret,
                                       attr, sizeof(attr), &attr_ret))) {
  	direction = SQL_FETCH_NEXT;
    fprintf(stderr, "%s - %s\n", driver, attr);
    if (ret == SQL_SUCCESS_WITH_INFO)
			fprintf(stderr, "\tdata truncation\n");
  }

	SQLFreeHandle(SQL_HANDLE_ENV, env);
}

void list_data_sources() {
	SQLHENV env;
	SQLAllocHandle(SQL_HANDLE_ENV, SQL_NULL_HANDLE, &env);
  SQLSetEnvAttr(env, SQL_ATTR_ODBC_VERSION, (void *) SQL_OV_ODBC3, 0);

	SQLSMALLINT direction = SQL_FETCH_FIRST;
	SQLCHAR dsn[256];
	SQLCHAR desc[256];
	SQLRETURN ret;
	SQLSMALLINT dsnResult;
	SQLSMALLINT descResult;
  while(SQL_SUCCEEDED(ret = SQLDataSources(env, direction,
                                       dsn, sizeof(dsn), &dsnResult,
                                       desc, sizeof(desc), &descResult))) {
  	direction = SQL_FETCH_NEXT;
    fprintf(stderr, "%s - %s\n", dsn, desc);
    if (ret == SQL_SUCCESS_WITH_INFO)
			fprintf(stderr, "\tdata truncation\n");
  }

	SQLFreeHandle(SQL_HANDLE_ENV, env);
}

void db_print_error(SQLHANDLE handle, SQLSMALLINT type)
{
	SQLINTEGER   i = 0;
	SQLINTEGER   native;
	SQLCHAR      state[ 7 ];
	SQLCHAR      text[256];
	SQLSMALLINT  len;
	SQLRETURN    ret;

	do
	{
		ret = SQLGetDiagRec(type, handle, ++i, state, &native, text, sizeof(text), &len );
		if (SQL_SUCCEEDED(ret))
			fprintf(stderr, "Error: %s:%i:%i:%s\n", state, i, native, text);
	}
	while(SQL_SUCCEEDED(ret));
}

