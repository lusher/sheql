#include "pbuffer.h"
#include <string.h>

void* pb_add(char** pbuffer, size_t* pbuffer_len, size_t* offset, const void* value, const size_t size) {
	const size_t alloc_length = 128;
	//void* current_val = 0;
	if (*pbuffer == NULL && size < alloc_length) {
		*pbuffer = malloc(size);
		//current_val = 0;
		*pbuffer_len = alloc_length;
	}
	else if (*pbuffer == NULL) {
		*pbuffer_len = size;
		*pbuffer = malloc(alloc_length);
	}
	else if (*pbuffer_len - *offset < size && size < alloc_length) {
		*pbuffer = realloc(pbuffer, *pbuffer_len + alloc_length);
		*pbuffer_len += alloc_length;
	}
	else if (*pbuffer_len - *offset < size) {
		*pbuffer = realloc(*pbuffer, *pbuffer_len + size);
		*pbuffer_len += size;
	}

	void* current = *pbuffer + *offset;
	memcpy(current, value, size);
	*offset += size;
	return current;
}




